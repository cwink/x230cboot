#!/bin/sh

set -e

trap cleanup EXIT INT HUP TERM

lsmod | grep -Eq "^lpc_ich" && haslpc=1 || haslpc=0

cleanup()
{
	[ "${haslpc}" -eq 1 ] && doas modprobe lpc_ich
}

[ "${haslpc}" -eq 1 ] && doas modprobe -r lpc_ich

doas flashrom -p internal --write "coreboot/build/coreboot.rom"
