#!/bin/sh

set -e

trap cleanup EXIT INT HUP TERM

origpwd="$(pwd)"

cleanup()
{
	cd "${origpwd}"
}

cd "coreboot"

make clean distclean

cd "${origpwd}"

mkdir -p "coreboot/3rdparty/blobs/mainboard/lenovo/x230"

cp -f "blobs/bios.bin" "blobs/descriptor.bin" "blobs/gbe.bin" "blobs/me.bin" \
	"coreboot/3rdparty/blobs/mainboard/lenovo/x230"

cp -f "config" "coreboot/.config"

cd "coreboot"

make olddefconfig

make
