# Thinkpad x230 Coreboot

This repository contains scripts, blobs, and configuration for building and
installing *coreboot* onto my Thinkpad x230.

**NOTE: The content of this repository targets my specific Thinkpad. I'm not
responsible for any misuse of this content.**

## Setup

To setup the repository initially, run [bootstrap.sh](bootstrap.sh):

```bash
./bootstrap.sh
```

This will download *coreboot*, extract the needed blobs from
[blobs.tar.gz](blobs.tar.gz), etc.

## Build

To build a `coreboot.rom` file, run the [build.sh](build.sh) script:

```bash
./build.sh
```

This will copy over the [config](config) file, update it with the latest
configuration, and build the ROM file.

## Flash

To flash the new `coreboot.rom` file, run the [flash.sh](flash.sh) script:

```bash
./flash.sh
```

This will simply flash the `coreboot.rom` file to the internal ROM chip.
