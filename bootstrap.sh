#!/bin/sh

set -e

trap cleanup EXIT INT HUP TERM

origpwd="$(pwd)"

cleanup()
{
	cd "${origpwd}"
}

git clone "https://review.coreboot.org/coreboot"

cd "coreboot"

git submodule update --init --recursive

numproc="$(nproc)"

make crossgcc-i386 CPUS="${numproc}"

cd "util/ifdtool"

make

cd "${origpwd}"

tar xvzf "blobs.tar.gz"
